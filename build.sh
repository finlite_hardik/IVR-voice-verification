#!/bin/sh

## Set global variables
ApplicationRegion="YOUR_REGION"  # AllowedPattern: [a-zA-Z0-9-]+
S3SourceBucket="YOUR_S3_BUCKET_WITH_SOURCE_CODE"  # AllowedPattern: [a-zA-Z0-9-]+
S3RecordingsBucketName="YOUR-S3-BUCKET_SAVING_AUDEO_RECORDING"  # AllowedPattern: [a-zA-Z0-9-]+
DynamoDBContactTableName="YOUR_MAIN_TABLE_NAME_TO_SAVE_CUSTOMER_DETAILS"  # AllowedPattern: [a-zA-Z0-9-]+
ContactTableIndexName="YOUR_MAIN_TABLE_INDEX_NAME"  # AllowedPattern: [a-zA-Z0-9-]+
DynamoDBContactHistoryTableName="YOUR_LOGS_TABLE_FOR_STORING_HISTORY_OF_MAIN_TABLE"  # AllowedPattern: [a-zA-Z0-9-]+
ContactHistoryTableIndexName="YOUR_LOGS_TABLE_INDEX_NAME"  # AllowedPattern: [a-zA-Z0-9-]+
SQSQueueName="YOUR_SQS_QUEUENAME"  # AllowedPattern: [a-zA-Z0-9]+
CloudFormationStack="YOUR_CLOUDFORMATION_STACK_NAME"
LambdaExtractAudioPolicyName="LAMBDA_POLICY_NAME_FOR_EXTRACT_AUDEO" # Name of the Audeo Stream extract policy.(Note: It will be unique)
CaptureStreamPolicyName="LAMBDA_POLICY_NAME_FOR_CAPTURE_STREAM" # Name of Capture stream policy.(Note: It will be unique)
StreamResultPolicyName="LAMBDA_POLICY_NAME_FOR_STREAM_RESULT" # Name of stream result policy.(Note: It will be unique)
GetCustomerAudeoClipPolicyName="LAMBDA_POLICY_NAME_FOR_GET_CUSTOMER_AUDEO_FROM_S3" # Name of policy for get custoemr audeo.(Note: It will be unique)
CheckCustomerEnrollmentPolicyName="LAMBDA_POLICY_NAME_FOR_CHECK_CUSTOMER_ENROLLMENT" # Name of policy for checking customer enrollment.(Note: It will be unique)

## Create S3 bucket and set parameters for CloudFormation stack
# Build your paramater string for the CFT
JSON_PARAM="ParameterKey=S3SourceBucket,ParameterValue=%s ParameterKey=DynamoDBContactTableName,ParameterValue=%s ParameterKey=ContactTableIndexName,ParameterValue=%s ParameterKey=DynamoDBContactHistoryTableName,ParameterValue=%s ParameterKey=ContactHistoryTableIndexName,ParameterValue=%s ParameterKey=S3RecordingsBucketName,ParameterValue=%s ParameterKey=SQSQueueName,ParameterValue=%s ParameterKey=LambdaExtractAudioPolicyName,ParameterValue=%s ParameterKey=CaptureStreamPolicyName,ParameterValue=%s ParameterKey=StreamResultPolicyName,ParameterValue=%s ParameterKey=GetCustomerAudeoClipPolicyName,ParameterValue=%s ParameterKey=CheckCustomerEnrollmentPolicyName,ParameterValue=%s"
JSON_PARAM=$(printf "$JSON_PARAM" "$S3SourceBucket" "$DynamoDBContactTableName" "$ContactTableIndexName" "$DynamoDBContactHistoryTableName" "$ContactHistoryTableIndexName" "$S3RecordingsBucketName" "$SQSQueueName" "$LambdaExtractAudioPolicyName" "$CaptureStreamPolicyName" "$StreamResultPolicyName" "$GetCustomerAudeoClipPolicyName" "$CheckCustomerEnrollmentPolicyName")

# Create your S3 bucket
if [ "$ApplicationRegion" = "us-east-1" ]; then
	aws s3api create-bucket --bucket $S3SourceBucket
else
	aws s3api create-bucket --bucket $S3SourceBucket --region $ApplicationRegion --create-bucket-configuration LocationConstraint=$ApplicationRegion
fi

## Code build and resource upload
# Build gradle project
echo "Build Gradle Project"
cd src/IVR-ExtractVoiceFromVideoStream/
gradle build
cd ../../

# Create resources
mkdir resources
echo "ZIP Python files"
files="capture_stream check_customer_enrollment getCustomerAudeoClip stream_result"
for file in $files
do
    output="../../resources/$file.zip"
    cd src/$file
    zip -r $output *.py
    cd ../../
done

echo "Copy Gradle Build to resources"
cp src/IVR-ExtractVoiceFromVideoStream/build/distributions/IVR-ExtractVoiceFromVideoStream.zip resources/IVR-ExtractVoiceFromVideoStream.zip

# Upload resources to S3
echo "Upload files to S3"
aws s3 cp cloudformation/customer-enrollment-stack.json s3://$S3SourceBucket
files="capture_stream check_customer_enrollment getCustomerAudeoClip stream_result IVR-ExtractVoiceFromVideoStream"
for file in $files
do
    input="resources/$file.zip"
    aws s3 cp $input s3://$S3SourceBucket
done
rm -rf resources

## Run CFT stack creation
aws cloudformation create-stack --stack-name $CloudFormationStack --template-url https://$S3SourceBucket.s3.$ApplicationRegion.amazonaws.com/customer-enrollment-stack.json --parameters $JSON_PARAM --capabilities CAPABILITY_NAMED_IAM --region $ApplicationRegion
