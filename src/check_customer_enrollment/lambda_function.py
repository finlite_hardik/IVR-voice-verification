from __future__ import print_function
import json
import boto3
import random
import datetime
from datetime import datetime
import requests
import os



API_KEY = os.environ['API_KEY']
DynamoDBContactTableName = os.environ['DynamoDBContactTableName']
DynamoDBContactHistoryTableName = os.environ['DynamoDBContactHistoryTableName']
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(DynamoDBContactTableName)
Customer_Call_History_table = dynamodb.Table(DynamoDBContactHistoryTableName)
lambda_client = boto3.client('lambda')

def lambda_handler(event, context):
    print('check_is_customer function invoked.')
    print('Event Parameters.')
    print(event)
    userId = None
    customer_number = event['Details']['Parameters']['customer_number']
    
    response = table.get_item(
      Key={
        'customer_number': customer_number
      }
    )
    
    if 'Item' in response:
      print("Customer Exists")
      item = response['Item']
      update_status(customer_number,event,item)
      create_customer_call_hisotry(customer_number,event,"CONTACT_UPDATED")
      
      if(item['enrolled'] == True):
        print("CUSTOMER ENROLMENT COMPLETED BEFORE")
        print("CALLING THE VOICEKEY API FOR GETTING THE CODE FOR THE VERIFICATION.")
        try:
          userId = item['voicekey_resp_id']
          print("USER ID ",userId)
        except:
          userId = None
          print("USER ID ",userId)
          print("IN EXCEPTION ENROLLMENT ID NOT FOUND.! SOMETHING WENT WRONG!")
          return {
            'enrolled': False
          }    
        print("SETTING THE CODE TO THE DYNAMODB WITH THE STATUS ENROLLMENT_VERIFICATION_STARTED.")
        voicekey_verification_details = getVoicekeyVerificationCode(customer_number,userId,event,item)
        try:
          return {
            'enrolled': item['enrolled'],
            'verify_code' : voicekey_verification_details['voicekey_verification_number']
          }
        except:
          return {
            'enrolled': item['enrolled'],
            'verify_code' : 2343
          }
        
      else:
        print("CUSTOMER ENROLMENT STATUS UNTRACEBLE OF PENDING")
        return {
          'enrolled': item['enrolled']
        }
    else:
      print("NEW CONTACT CREATED.!")
      create_customer(customer_number,event)
      create_customer_call_hisotry(customer_number,event,"CONTACT_CREATED")
      return {
        'enrolled': False
      }

def getVoicekeyVerificationCode(customer_number,userId,event,item):
  print("WE'RE IN THE METHOD GET_VOUCEKEYE_RIFICATION_CODE")
  url = "http://ec2-3-108-145-48.ap-south-1.compute.amazonaws.com:8080/api/v3/getNumber"
  print("API KEY : ", API_KEY)
  payload={
    'language': 'en',
    'userid': userId
  }
  
  files=[]
  headers = {
    "api-key": API_KEY
	}
  resp = requests.request("POST", url, headers=headers, data=payload, files=files)
  print(resp.text)
  voicekey_resp = json.loads(resp.text)
  try:
	  voicekey_verification_number = voicekey_resp['number'] if voicekey_resp['number'] !=None else ""
  except:
    voicekey_verification_number = ""
  print("VOICEKEY VERIFICATION CODE ",voicekey_verification_number)
  print("VOICEKEY VERIFICATION CODE SET IN DYNAMODB AND UPADATE THE STATUS")
  update_verification_status(customer_number,event,item,voicekey_verification_number)
  print("CONTACT VERIFICATION CODE AND SET THE SETATUS in DYNAMODB SUCCESSFULLY.!")
  return {
    "voicekey_verification_number" : voicekey_verification_number
  }
  
  
def create_customer(customer_number,event):
    print("Creating new customer: " + customer_number)
    table.put_item(
     Item={
        'customer_number': customer_number,
        'ContactId' : event['Details']['ContactData']['ContactId'],
        'previousContactIds' : [event['Details']['ContactData']['ContactId']],
        'enrolled': False,
        'enrol_state': 'CONTACT_CREATED'
      }
    )
    
def create_customer_call_hisotry(customer_number,event,status = "CONTACT_CREATED"):
    now = datetime.now()
    createdDate = now.strftime("%m/%d/%Y, %H:%M:%S")
    print("Creating new customer Call history: " + customer_number)
    Customer_Call_History_table.put_item(
     Item={
        'customer_number': customer_number,
        'ContactId' : event['Details']['ContactData']['ContactId'],
        'status': status,
        "createdDate" : createdDate
      }
    )
    
def update_status(customer_number,event,item):
    print("Updating status")
    try:
      previousContactIds = item['previousContactIds']
    except:
      previousContactIds = []
    
    previousContactIds.append(event['Details']['ContactData']['ContactId'])
    response = table.update_item(
      Key={
          'customer_number': customer_number
      },
      UpdateExpression='SET ContactId = :ContactId,previousContactIds = :previousContactIds,enrol_state = :await',
      ExpressionAttributeValues={
          ":ContactId" : event['Details']['ContactData']['ContactId'],
          ":previousContactIds" : previousContactIds,
          ':await': 'CONTACT_UPDATED'
      }
    )

def update_verification_status(customer_number,event,item,voicekey_verification_number):
    print("IN VEOICEKEY VERIFICATION STATUS UPDATE METHOD.!")
    print("VOICEKEY VERIFICATION CODE SETTING IN DYNAMODB AND UPADATING THE STATUS")
    
    response = table.update_item(
      Key={
          'customer_number': customer_number
      },
      UpdateExpression='SET enrol_state = :await,voicekey_verification_number = :voicekey_verification_number',
      ExpressionAttributeValues={
          ':await': 'ENROLLMENT_VERIFICATION_STARTED',
          ':voicekey_verification_number': voicekey_verification_number
      }
    )
    print("VOICEKEY VERIFICATION CODE SET IN DYNAMODB AND UPADATAED THE STATUS")
    create_customer_call_hisotry(customer_number,event,"ENROLLMENT_VERIFICATION_STARTED")
    print("CONTACT CREATED FOR VOICEKEY VERIFICATION CODE SET IN DYNAMODB AND UPADATAED THE STATUS.!")
    