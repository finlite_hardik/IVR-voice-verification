from __future__ import print_function
import json
import boto3
import os
from datetime import datetime


# Get SQS client and queue url
queue_url = os.environ['QUEUE_URL']
DynamoDBContactTableName = os.environ['DynamoDBContactTableName']
DynamoDBContactHistoryTableName = os.environ['DynamoDBContactHistoryTableName']
client = boto3.client('sqs')
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(DynamoDBContactTableName)
Customer_Call_History_table = dynamodb.Table(DynamoDBContactHistoryTableName)
# lambda_client = boto3.client('lambda')


def lambda_handler(event, context):
    print('capture_stream function invoked.')
    print('Event Parameters.')
    print(event)
    customer_number = event['Details']['Parameters']['customer_number']
    stream = event['Details']['Parameters']['stream']
    command = event['Details']['Parameters']['command']
    start_time = event['Details']['Parameters']['start_time']
    stop_time = event['Details']['Parameters']['stop_time']
    call_id = context.aws_request_id

    if(command == "verify"):
        response = client.send_message(
            MessageBody = json.dumps(event, separators=(',', ':')),
            DelaySeconds = 45,
            MessageAttributes = {
                'StreamARN': {
                    'StringValue': event["Details"]["ContactData"]["MediaStreams"]["Customer"]["Audio"]["StreamARN"],
                    'DataType': 'String'
                },
                'ContactId': {
                    'StringValue': event["Details"]["ContactData"]["ContactId"],
                    'DataType': 'String'
                },
                'StartFragmentNumber': {
                    'StringValue': event["Details"]["ContactData"]["MediaStreams"]["Customer"]["Audio"]["StartFragmentNumber"],
                    'DataType': 'String'
                },
                'CustomerEndpointAddress': {
                    'StringValue': event["Details"]["ContactData"]["CustomerEndpoint"]["Address"],
                    'DataType': 'String'
                },
                'SystemEndpointAddress': {
                    'StringValue': event["Details"]["ContactData"]["SystemEndpoint"]["Address"],
                    'DataType': 'String'
                }
            },
            QueueUrl = queue_url
        )
        
        update_status(customer_number,'ENROLLMENT_VERIFICATION_STREAM_RECORDING_INPROGRESS')
        create_customer_call_hisotry(customer_number,event,'ENROLLMENT_VERIFICATION_STREAM_RECORDING_INPROGRESS')
        return {
            "success": True
        }
    else:
        response = client.send_message(
            MessageBody = json.dumps(event, separators=(',', ':')),
            DelaySeconds = 45,
            MessageAttributes = {
                'StreamARN': {
                    'StringValue': event["Details"]["ContactData"]["MediaStreams"]["Customer"]["Audio"]["StreamARN"],
                    'DataType': 'String'
                },
                'ContactId': {
                    'StringValue': event["Details"]["ContactData"]["ContactId"],
                    'DataType': 'String'
                },
                'StartFragmentNumber': {
                    'StringValue': event["Details"]["ContactData"]["MediaStreams"]["Customer"]["Audio"]["StartFragmentNumber"],
                    'DataType': 'String'
                },
                'CustomerEndpointAddress': {
                    'StringValue': event["Details"]["ContactData"]["CustomerEndpoint"]["Address"],
                    'DataType': 'String'
                },
                'SystemEndpointAddress': {
                    'StringValue': event["Details"]["ContactData"]["SystemEndpoint"]["Address"],
                    'DataType': 'String'
                }
            },
            QueueUrl = queue_url
        )
        
        update_status(customer_number,'ENROLMENT_CLIP_RECORDING_IN_PROGRESS')
        create_customer_call_hisotry(customer_number,event,'ENROLMENT_CLIP_RECORDING_IN_PROGRESS')
        return {
            "success": True
        }

def create_customer(customer_number,event,status,state_remark):
    print("Creating new customer: " + customer_number)
    table.put_item(
     Item={
        'customer_number': customer_number,
        'ContactId' : event['Details']['ContactData']['ContactId'],
        'enrolled': status,
        'enrol_state': state_remark
      }
    )
    
def update_status(customer_number,state_remark):
    print("Updating status")
    response = table.update_item(
      Key={
          'customer_number': customer_number
      },
      UpdateExpression='SET enrol_state = :await',
      ExpressionAttributeValues={
          ':await': state_remark
      }
    )
def create_customer_call_hisotry(customer_number,event,state_remark):
    now = datetime.now()
    createdDate = now.strftime("%m/%d/%Y, %H:%M:%S")
    print("Creating new customer Call history: " + customer_number)
    Customer_Call_History_table.put_item(
     Item={
        'customer_number': customer_number,
        'ContactId' : event['Details']['ContactData']['ContactId'],
        'status': state_remark,
        "createdDate" : createdDate
      }
    )