import boto3
import json
import os
import datetime
from datetime import datetime
from os.path import exists
import tempfile
from boto3.s3.transfer import TransferConfig
import requests
import wave


API_KEY = os.environ['API_KEY']
DynamoDBContactTableName = os.environ['DynamoDBContactTableName']
DynamoDBContactHistoryTableName = os.environ['DynamoDBContactHistoryTableName']
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(DynamoDBContactTableName)
Customer_Call_History_table = dynamodb.Table(DynamoDBContactHistoryTableName)

def get_s3_bucket():
    s3 = boto3.client("s3")
    return s3
    
def get_s3_object(event,s3):
    final_file_name = ''
    file_content = 'No content in file'
    file_json_content = {}
    contact_id = ''
    bucketname = ''
    """Read file from s3 on trigger."""
    if event:
        print("get s3 bucket call")
        print(event)
        file_obj = event["Records"][0]
        bucketname = str(file_obj['s3']['bucket']['name'])
        filename = str(file_obj['s3']['object']['key'])
        # Set the desired multipart threshold value (5GB)
        GB = 1024 ** 3
        config = TransferConfig(multipart_threshold=5*GB)
        filePath = '/tmp/'+filename
        s3.download_file(bucketname, filename, filePath, Config=config)
        file_exists = exists('/tmp/'+filename)
        print("File Check Here")
        print(file_exists)
        print("File uploaded.!")
        
        try:
            FileParts = filename.split(".")
            ContactIdFile = FileParts[0]
            ContactIdFileName = filename
            localFilePath = filePath
        except:
            ContactIdFile = "dmmy_contactId"
            ContactIdFileName = "dmmy_contactId.wav"
            localFilePath = "UNDEFINED"
        print("in Get S3 Object Method : Contact ID to be updated with")
        print( "in Get S3 Object Method : ", ContactIdFile)
        print("in Get S3 Object Method : Local File Path Updated in Dynamodb ", localFilePath )
        
        response = Customer_Call_History_table.get_item(
          Key={
            "ContactId": ContactIdFile
          }
        )
        print("in Get S3 Object Method : The Contact Call Logs")
        print("in Get S3 Object Method : ", response['Item'])
        item = []
        customer_number = ""
        if 'Item' in response:
            print("in Get S3 Object Method : Customer call history Exists")
            item = response['Item']
            customer_number = item['customer_number']
        print("in Get S3 Object Method : Contact Number received")
        print("in Get S3 Object Method : ", customer_number)
        
        contact_resp = table.get_item(
          Key={
            'customer_number': customer_number
          }
        )
        
        if 'Item' in contact_resp:
            print("in Get S3 Object Method : Customer call history Exists")
            contact_item = contact_resp['Item']
        
        if(contact_item['enrolled'] == True):
            print("in Get S3 Object Method : verification state is ready.")
            # times between which to extract the wave from
            start = 8 # seconds
            end = 12 # seconds
            # file to extract the snippet from
            trimmedFile = ContactIdFile+"_trimmed.wav"
            trimmedFilePath = '/tmp/'+trimmedFile
            with wave.open(filePath, "rb") as infile:
                # get file data
                nchannels = infile.getnchannels()
                sampwidth = infile.getsampwidth()
                framerate = infile.getframerate()
                # set position in wave to start of segment
                infile.setpos(int(start * framerate))
                # extract data
                data = infile.readframes(int((end - start) * framerate))
            
            # write the extracted data to a new file
            with wave.open(filePath, 'w') as outfile:
                outfile.setnchannels(nchannels)
                outfile.setsampwidth(sampwidth)
                outfile.setframerate(framerate)
                outfile.setnframes(int(len(data) / sampwidth))
                outfile.writeframes(data)
            
            trim_file_exists = exists(filePath)
            print("Trimmed File Check Here")
            print(filePath)
            print("Trimmed File uploaded.!")
            
        return {
            "Filename" : filename,
            'filePath' : filePath
        }
    

def update_status(customer_number,ContactIdFile,item,enrol_state):
    print("Updating status")
    try:
      previousContactIds = item['previousContactIds']
    except:
      previousContactIds = []
    
    previousContactIds.append(ContactIdFile)
    response = table.update_item(
      Key={
          'customer_number': customer_number
      },
      UpdateExpression='SET ContactId = :ContactId,previousContactIds = :previousContactIds,enrol_state = :await',
      ExpressionAttributeValues={
          ":ContactId" : ContactIdFile,
          ":previousContactIds" : previousContactIds,
          ':await': enrol_state
      }
    )
    
def update_status_recorded_file(fileDetails):
    try:
        FileParts = fileDetails['Filename'].split(".")
        ContactIdFile = FileParts[0]
        ContactIdFileName = fileDetails['Filename']
        localFilePath = fileDetails['filePath']
    except:
        ContactIdFile = "dmmy_contactId"
        ContactIdFileName = "dmmy_contactId.wav"
        localFilePath = "UNDEFINED"
    print("Contact ID to be updated with")
    print(ContactIdFile)
    print("Local File Path Updated in Dynamodb ", localFilePath )
    
    response = Customer_Call_History_table.get_item(
      Key={
        "ContactId": ContactIdFile
      }
    )
    print("The Contact Call Logs")
    print(response['Item'])
    item = []
    customer_number = ""
    if 'Item' in response:
        print("Customer call history Exists")
        item = response['Item']
        customer_number = item['customer_number']
    print("Contact Number received")
    print(customer_number)
    
    contact_resp = table.get_item(
      Key={
        'customer_number': customer_number
      }
    )
    
    if 'Item' in contact_resp:
        print("Customer call history Exists")
        contact_item = contact_resp['Item']
    
    if(contact_item['enrolled'] == True):
        update_status(customer_number,ContactIdFile,item,'ENROLLMENT_VERIFICATION_STREAM_RECORDED')
        response = table.update_item(
              Key={
                  'customer_number': customer_number
              },
              UpdateExpression='SET enrollmentAudioClip = :enrollmentAudioClip,enrol_state = :await,localFilePath = :localFilePath',
              ExpressionAttributeValues={
                  ':await': 'ENROLLMENT_VERIFICATION_STREAM_RECORDED',
                  ":enrollmentAudioClip" : ContactIdFileName,
                  ":localFilePath" : localFilePath
              }
            )
        create_customer_call_hisotry(customer_number,ContactIdFile,'ENROLLMENT_VERIFICATION_STREAM_RECORDED')
    else:
        update_status(customer_number,ContactIdFile,item,'ENROLLMENT_CLIP_RECORDED')
        response = table.update_item(
              Key={
                  'customer_number': customer_number
              },
              UpdateExpression='SET enrollmentAudioClip = :enrollmentAudioClip,enrol_state = :await,localFilePath = :localFilePath',
              ExpressionAttributeValues={
                  ':await': 'ENROLLMENT_CLIP_RECORDED',
                  ":enrollmentAudioClip" : ContactIdFileName,
                  ":localFilePath" : localFilePath
              }
            )
        create_customer_call_hisotry(customer_number,ContactIdFile,'ENROLLMENT_CLIP_RECORDED')
    
    
    response = table.get_item(
      Key={
        'customer_number': customer_number
      }
    )
    try:
        if 'Item' in response:
            item = response['Item']
            enrol_state = item['enrol_state']
            enrolled = item['enrolled']
            userId = item['voicekey_resp_id']
    except:
        enrol_state = "UNTRACEBLE_STATE"
        enrolled = False
        userId = None
    
    return {
        "enrollmentAudioClip" : ContactIdFileName,
        'customer_number': customer_number,
        "ContactId": ContactIdFile,
        "enrolled" : enrolled,
        "enrol_state" : enrol_state,
        "userId" : userId,
        'item' : item
    }


def create_customer_call_hisotry(customer_number,ContactIdFile,enrol_state):
    now = datetime.now()
    createdDate = now.strftime("%m/%d/%Y, %H:%M:%S")
    print("Creating new customer call history: " + customer_number)
    Customer_Call_History_table.put_item(
     Item={
        'customer_number': customer_number,
        'ContactId' : ContactIdFile,
        'status': enrol_state,
        "createdDate" : createdDate
      }
    )
    
def lambda_handler(event, context):
    s3 = get_s3_bucket()
    response = get_s3_object(event,s3)
    print("s3 object result")
    print(response)
    update_result = update_status_recorded_file(response)
    print("s3 object updated awith the status")
    print(update_result)
    if(update_result['enrolled'] == True):
        if(update_result['enrol_state'] == "ENROLLMENT_VERIFICATION_STREAM_RECORDED"):
            print("Updating the status on the contactID" +  update_result['customer_number'] + "With The status " + "ENROLLMENT_VERIFICATION_STREAM_IN_PROGRESS")
            update_status(update_result['customer_number'],update_result['ContactId'],update_result['item'],"ENROLLMENT_VERIFICATION_STREAM_IN_PROGRESS")
            statusResponse = table.update_item(
                  Key={
                      'customer_number': update_result['customer_number']
                  },
                  UpdateExpression='SET enrol_state = :await',
                  ExpressionAttributeValues={
                      ':await': 'ENROLLMENT_VERIFICATION_STREAM_IN_PROGRESS'
                  }
                )
            print("Creating the contact history the status on the contactID" + update_result['customer_number'] + "With The status " + "ENROLLMENT_VERIFICATION_STREAM_IN_PROGRESS" + "for the contact ID " + update_result['ContactId'])
            create_customer_call_hisotry(update_result['customer_number'],update_result['ContactId'],"ENROLLMENT_VERIFICATION_STREAM_IN_PROGRESS")
        VoiceVerificationProcess(update_result['enrollmentAudioClip'],response['filePath'],update_result)
    else:
        if(update_result['enrol_state'] == "ENROLLMENT_CLIP_RECORDED"):
            print("Updating the status on the contactID" +  update_result['customer_number'] + "With The status " + "ENROLMENT_CLIP_ENROLLMENT_IN_PROGRESS")
            update_status(update_result['customer_number'],update_result['ContactId'],update_result['item'],"ENROLMENT_CLIP_ENROLLMENT_IN_PROGRESS")
            statusResponse = table.update_item(
                  Key={
                      'customer_number': update_result['customer_number']
                  },
                  UpdateExpression='SET enrol_state = :await',
                  ExpressionAttributeValues={
                      ':await': 'ENROLMENT_CLIP_ENROLLMENT_IN_PROGRESS'
                  }
                )
            print("Creating the contact history the status on the contactID" + update_result['customer_number'] + "With The status " + "ENROLMENT_CLIP_ENROLLMENT_IN_PROGRESS" + "for the contact ID " + update_result['ContactId'])
            create_customer_call_hisotry(update_result['customer_number'],update_result['ContactId'],"ENROLMENT_CLIP_ENROLLMENT_IN_PROGRESS")
        VoiceEnrollmentProcess(update_result['enrollmentAudioClip'],response['filePath'],update_result)
    

def VoiceEnrollmentProcess(enrollmentAudioClip,filePath,update_result):
    print("In the Voicekey nerollment Method.. ! Please sit bhind Enrollment in progress")
    print("API KEY : ", API_KEY)
    
    url = "http://ec2-3-108-145-48.ap-south-1.compute.amazonaws.com:8080/api/v3/enrollUser"
    print("URL : ", url)
    payload={'firstName': 'Hardik',
    'lastName': 'Shah',
    'language': 'en'}
    files=[
      ('audiofile1',(enrollmentAudioClip,open(filePath,'rb'),'audio/wav')),
      ('audiofile2',(enrollmentAudioClip,open(filePath,'rb'),'audio/wav')),
      ('audiofile3',(enrollmentAudioClip,open(filePath,'rb'),'audio/wav'))
    ]
    headers = {
      'api-key': API_KEY
    }
    
    resp = requests.request("POST", url, headers=headers, data=payload, files=files)
    print(resp.text)
    voicekey_resp = json.loads(resp.text)
    try:
        voicekey_resp_status = voicekey_resp['status'] if voicekey_resp['status'] !=None else ""
    except:
        voicekey_resp_status = ""
    try:
        voicekey_resp_id = voicekey_resp['id'] if voicekey_resp['id'] !=None else ""
    except:
        voicekey_resp_id = ""
    try:
        voicekey_resp_confidence = voicekey_resp['confidence'] if voicekey_resp['confidence'] !=None else ""
    except:
        voicekey_resp_confidence = ""
    
    if(voicekey_resp_status == "Enrolled successfully"):
        response = table.update_item(
          Key={
              'customer_number': update_result['customer_number']
          },
          UpdateExpression='SET enrol_state = :await,voicekey_resp_id = :voicekey_resp_id,voicekey_resp_confidence = :voicekey_resp_confidence,voicekey_resp_status = :voicekey_resp_status,enrolled = :enrolled,voicekey_response = :voicekey_result',
          ExpressionAttributeValues={
              ':await': 'ENROLMENT_CLIP_ENROLLMENT_COMPLETED',
              ':voicekey_resp_id': voicekey_resp_id,
              ':voicekey_resp_confidence': voicekey_resp_confidence,
              ':voicekey_resp_status': voicekey_resp_status,
              ':enrolled': True,
              ':voicekey_result': resp.text
          }
        )
        print("Creating the contact history the status on the contactID" + update_result['customer_number'] + "With The status " + "ENROLMENT_CLIP_ENROLLMENT_COMPLETED" + "for the contact ID " + update_result['ContactId'])
        now = datetime.now()
        createdDate = now.strftime("%m/%d/%Y, %H:%M:%S")
        print("Creating new customer call history: " + update_result['customer_number'])
        Customer_Call_History_table.put_item(
         Item={
            'customer_number': update_result["customer_number"],
            'ContactId' : update_result["ContactId"],
            'status': "ENROLMENT_CLIP_ENROLLMENT_COMPLETED",
            'voicekey_resp_id': voicekey_resp_id,
            'voicekey_resp_confidence': voicekey_resp_confidence,
            'voicekey_resp_status': voicekey_resp_status,
            'enrolled': True,
            'voicekey_result': resp.text,
            "createdDate" : createdDate
          }
        )
    else:
        response = table.update_item(
          Key={
              'customer_number': update_result['customer_number']
          },
          UpdateExpression='SET enrol_state = :await,voicekey_resp_id = :voicekey_resp_id,voicekey_resp_confidence = :voicekey_resp_confidence,voicekey_resp_status = :voicekey_resp_status,enrolled = :enrolled,voicekey_response = :voicekey_result',
          ExpressionAttributeValues={
              ':await': 'ENROLMENT_CLIP_ENROLLMENT_FAILED',
              ':voicekey_resp_id': voicekey_resp_id,
              ':voicekey_resp_confidence': voicekey_resp_confidence,
              ':voicekey_resp_status': voicekey_resp_status,
              ':enrolled': False,
              ':voicekey_result': resp.text
          }
        )
        print("Creating the contact history the status on the contactID" + update_result['customer_number'] + "With The status " + "ENROLMENT_CLIP_ENROLLMENT_COMPLETED" + "for the contact ID " + update_result['ContactId'])
        now = datetime.now()
        createdDate = now.strftime("%m/%d/%Y, %H:%M:%S")
        print("Creating new customer call history: " + update_result['customer_number'])
        Customer_Call_History_table.put_item(
         Item={
            'customer_number': update_result["customer_number"],
            'ContactId' : update_result["ContactId"],
            'status': "ENROLMENT_CLIP_ENROLLMENT_FAILED",
            'voicekey_resp_id': voicekey_resp_id,
            'voicekey_resp_confidence': voicekey_resp_confidence,
            'voicekey_resp_status': voicekey_resp_status,
            'enrolled': False,
            'voicekey_result': resp.text,
            "createdDate" : createdDate
          }
        )
        
def VoiceVerificationProcess(enrollmentAudioClip,filePath,update_result):
    print("In the Voicekey verification Method.. ! Please sit bhind verification in progress")
    print("API KEY : ", API_KEY)
    
    url = "http://ec2-3-108-145-48.ap-south-1.compute.amazonaws.com:8080/api/v3/verifyUser"
    print("URL : ", url)
    payload={'userid': update_result['userId'],
    'language': 'en'}
    files=[
      ('audiofile',(enrollmentAudioClip,open(filePath,'rb'),'audio/wav'))
    ]
    headers = {
      'api-key': API_KEY
    }
    
    resp = requests.request("POST", url, headers=headers, data=payload, files=files)
    print(resp.text)
    voicekey_resp = json.loads(resp.text)
    try:
        voicekey_resp_verification_status = voicekey_resp['status'] if voicekey_resp['status'] !=None else ""
    except:
        voicekey_resp_verification_status = ""
    try:
        voicekey_resp_id = voicekey_resp['user_id'] if voicekey_resp['user_id'] !=None else ""
    except:
        voicekey_resp_id = ""
    try:
        voicekey_resp_similarity_score = voicekey_resp['similarity_score'] if voicekey_resp['similarity_score'] !=None else ""
    except:
        voicekey_resp_similarity_score = ""
    try:
        voicekey_resp_threshold = voicekey_resp['threshold'] if voicekey_resp['threshold'] !=None else ""
    except:
        voicekey_resp_threshold = ""
    try:
        verified = voicekey_resp['success'] if voicekey_resp['success'] is True else False
    except:
        verified = False
    
    if(verified is True):
        response = table.update_item(
          Key={
              'customer_number': update_result['customer_number']
          },
          UpdateExpression='SET enrol_state = :await,voicekey_resp_id = :voicekey_resp_id,voicekey_resp_similarity_score = :voicekey_resp_similarity_score,voicekey_resp_verification_status = :voicekey_resp_verification_status,verified = :verified,voicekey_response = :voicekey_result',
          ExpressionAttributeValues={
              ':await': 'ENROLLMENT_VERIFICATION_STREAM_COMPLETED',
              ':voicekey_resp_id': voicekey_resp_id,
              ':voicekey_resp_similarity_score': voicekey_resp_similarity_score,
              ':voicekey_resp_verification_status': voicekey_resp_verification_status,
              ':verified': verified,
              ':voicekey_result': resp.text
          }
        )
        print("Creating the contact history the status on the contactID" + update_result['customer_number'] + "With The status " + "ENROLLMENT_VERIFICATION_STREAM_COMPLETED" + "for the contact ID " + update_result['ContactId'])
        now = datetime.now()
        createdDate = now.strftime("%m/%d/%Y, %H:%M:%S")
        print("Creating new customer call history: " + update_result['customer_number'])
        Customer_Call_History_table.put_item(
         Item={
            'customer_number': update_result["customer_number"],
            'ContactId' : update_result["ContactId"],
            'status': "ENROLLMENT_VERIFICATION_STREAM_COMPLETED",
            'voicekey_resp_id': voicekey_resp_id,
            'voicekey_resp_similarity_score': voicekey_resp_similarity_score,
            'voicekey_resp_verification_status': voicekey_resp_verification_status,
            'verified': verified,
            'voicekey_result': resp.text,
            "createdDate" : createdDate
          }
        )
    else:
        response = table.update_item(
          Key={
              'customer_number': update_result['customer_number']
          },
          UpdateExpression='SET enrol_state = :await,voicekey_resp_id = :voicekey_resp_id,voicekey_resp_similarity_score = :voicekey_resp_similarity_score,voicekey_resp_verification_status = :voicekey_resp_verification_status,verified = :verified,voicekey_response = :voicekey_result',
          ExpressionAttributeValues={
              ':await': 'ENROLLMENT_VERIFICATION_STREAM_FAILED',
              ':voicekey_resp_id': voicekey_resp_id,
              ':voicekey_resp_similarity_score': voicekey_resp_similarity_score,
              ':voicekey_resp_verification_status': voicekey_resp_verification_status,
              ':verified': verified,
              ':voicekey_result': resp.text
          }
        )
        print("Creating the contact history the status on the contactID" + update_result['customer_number'] + "With The status " + "ENROLLMENT_VERIFICATION_STREAM_FAILED" + "for the contact ID " + update_result['ContactId'])
        now = datetime.now()
        createdDate = now.strftime("%m/%d/%Y, %H:%M:%S")
        print("Creating new customer call history: " + update_result['customer_number'])
        Customer_Call_History_table.put_item(
         Item={
            'customer_number': update_result["customer_number"],
            'ContactId' : update_result["ContactId"],
            'status': "ENROLLMENT_VERIFICATION_STREAM_FAILED",
            'voicekey_resp_id': voicekey_resp_id,
            'voicekey_resp_similarity_score': voicekey_resp_similarity_score,
            'voicekey_resp_verification_status': voicekey_resp_verification_status,
            'verified': verified,
            'voicekey_result': resp.text,
            "createdDate" : createdDate
          }
        )
    
    
    

    
    
    
    