from __future__ import print_function
import json
import boto3
import random
import os


DynamoDBContactTableName = os.environ['DynamoDBContactTableName']
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(DynamoDBContactTableName)

def lambda_handler(event, context):
    print('verify_stream_result function invoked.')
    print('Event Parameters.')
    print(event)
    
    customer_number = event['Details']['Parameters']['customer_number']
    command = event['Details']['Parameters']['command']
    
    response = table.get_item(
      Key={
        'customer_number': customer_number
      }
    )
    
    item = response['Item']
    if(command == "enrol_result"):
        print({
          'enrol_state': item['enrol_state']
        })
        return {
          'enrol_state': item['enrol_state']
        }
    
    if(command == "verify_result"):
        print({
          'enrol_state': item['enrol_state']
        })
        return {
          'enrol_state': item['enrol_state']
        }